<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {return view('pages.home');});

/*Route::group(['middleware' => ['auth']], function() {

});

Route::get('/register', 'UserController@showRegister');

Route::post('/register', 'UserController@register');

Route::get('/login', 'UserController@showLogin');

Route::post('/login', 'UserController@login');*/

Auth::routes(['verify' => true]);

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/', 'HomeController@index')->name('home');

Route::get('cities', 'CityController@index')->name('cities.index');
Route::get('states', 'StateController@index')->name('states.index');
