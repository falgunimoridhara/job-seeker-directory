<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UserController extends Controller
{
    public function showRegister()
    {
        return view('pages.register');
    }

    public function register(Request $request)
    {
        //how to create custom validation rules
        //https://dev.to/rizwan_saquib/create--manage-user-registration-system-with-laravel-queues--supervisor-nl
    	$this->validate($request,[
    			'first-name' => 'required|min:3',
    			'last-name' => 'required|min:3',
    			'email' => 'required|email|unique:users',
    			'password' => 'required|min:6|max:20',
    			'confirm-password' => 'required|min:6|max:20|same:password',
    		],[
    			'first-name.required' => trans('messages.first_name_missing_error'),
    			'first-name.min' => 'The first name must be at least 5 characters.',
    			'first-name.max' => 'The first name may not be greater than 35 characters.',
    			'last-name.required' => 'The last name field is required.',
    			'last-name.min' => 'The last name must be at least 5 characters.',
    			'last-name.max' => 'The last name may not be greater than 35 characters.',
    		]);

        $user = new User([
            'first_name' => $request->input('first-name'),
            'last_name' => $request->input('last-name'),
            'email' => $request->input('email'),
            'linkedin_profile_url' => $request->input('linkedin-profile'),
            'password' => bcrypt($request->input('password')),
            'layoff_date' => $request->input('layoff-date'),
            'total_experience' => $request->input('total-experience'),
        ]);

        $user->save();
        return response()->json([
            'message' => 'user successfully created!'
        ], 201);
    }

    function showLogin()
    {
        return view('pages.login');
    }

    function login(Request $request)
    {

    }
}
