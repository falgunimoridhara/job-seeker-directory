<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class CityController extends Controller
{
    public function index()
    {
        $cities = DB::table('cities')
                ->where('id_state', request()->input('state', 0))
                ->pluck('city', 'id');
        return response()->json($cities);
    }
}
