<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class StateController extends Controller
{
    public function index()
    {
        $states = DB::table('states')
                ->where('id_country', request()->input('country', 0))
                ->pluck('state', 'id');
        return response()->json($states);
    }
}
