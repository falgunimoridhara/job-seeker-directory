<?php

namespace App\Http\Controllers\Auth;

use App\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/'; //home

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first-name' => 'required|min:3',
            'last-name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|max:20',
            'confirm-password' => 'required|min:6|max:20|same:password',
            'company' => 'required',
            'skill' => 'required',
            'job-title' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'resume' => 'required|mimes:pdf,doc|max:2048',
        ],[
            'first-name.required' => trans('messages.first_name_missing_error'),
            'first-name.min' => 'The first name must be at least 5 characters.',
            'first-name.max' => 'The first name may not be greater than 35 characters.',
            'last-name.required' => 'The last name field is required.',
            'last-name.min' => 'The last name must be at least 5 characters.',
            'last-name.max' => 'The last name may not be greater than 35 characters.',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $path = Request::file('resume')->store('resume');

        return User::create([
            'first_name' => $data['first-name'],
            'last_name' => $data['last-name'],
            'email' => $data['email'],
            'linkedin_profile_url' => $data['linkedin-profile'],
            'password' => Hash::make($data['password']),
            'layoff_date' => $data['layoff-date'],
            'total_experience' => $data['total-experience'],
            'id_company' => $data['company'],
            'id_city' => $data['city'],
            'id_skill' => $data['skill'],
            'id_job_title' => $data['job-title'],
            'offline_resume' => $path,
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $countries = DB::table('countries')->get();
        $companies = DB::table('companies')->get();
        $jobTitles = DB::table('job_titles')->get();
        $skills = DB::table('skills')->get();
        return view('auth.register', compact('countries', 'companies', 'jobTitles', 'skills'));
    }
}
