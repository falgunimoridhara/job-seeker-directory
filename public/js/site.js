function addErr(errId, errMsg) {
    $('#' + errId).addClass('form_err');

    var spanErrId = 'err_' + errId;

    if (errMsg == '' || errMsg == null) {
        errMsg = 'You can\'t leave this empty';
    }

    if ($('#' + spanErrId).length) {
        var em = document.getElementById(spanErrId);
		$(em).children().remove();
    }

    var em = document.createElement('span');
    em.id = spanErrId;
    var errDispId = document.getElementById(errId);

	$(em).html('<span class="errmsg">'+errMsg+'</span>');

    errDispId.parentNode.insertBefore(em, errDispId.nextSibling);

    return false;
}

function removeErr(errId) {
    $('#' + errId).removeClass('form_err');
    $('#err_' + errId).remove();
}

function validateEmail(email) {
    var emailPattern = /^([A-Za-z0-9_\-\.\+])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,10})$/;
    return emailPattern.test($.trim(email));
}

function checkField(inputId) {
    totalErr = 0;
    var nmRegPattern = /^[A-Za-z0-9_]+$/;
    if (inputId == 'email' || inputId == 'frgt-pwd-email') {
        if ($.trim($('#' + inputId).val()) == '') {
            addErr(inputId, 'Please enter your email');
            totalErr++;
        }
        else if (validateEmail($('#' + inputId).val()) == false) {
            addErr(inputId, 'Please enter a valid email');
             totalErr++;
        }
        else {
            removeErr(inputId);
        }
    }

    if (inputId == 'password' || inputId == 'sign-up-password' || inputId == 'reset-password') {
        if ($.trim($('#' + inputId).val()) == '') {
            addErr(inputId, 'Please enter your password');
            totalErr++;
        }
        else if (($.trim($('#' + inputId).val()).length < 6 || $.trim($('#'+inputId).val()).length > 20)) {
            addErr(inputId, 'Password length should be between 6 to 20 characters');
            totalErr++;
        }
        else {
            removeErr(inputId);
        }
    }

    return totalErr;
}

function userSignUp() {
    totalErr = checkField('email');
    totalErr += checkField('password');

    if(totalErr == 0) {
        return true;
    }
    return false;
}
