<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Site captions
    |--------------------------------------------------------------------------
    |
    */

    'email' => 'Email',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'linkedin_profile_url' => 'LinkedIn Profile Url',
    'layoff_date' => 'Layoff Date',
    'total_experience' => 'Total Experience',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',

    'first_name_missing_error' => 'Please enter first name',
    'last_name_missing_error' => 'Please enter last name',

];
