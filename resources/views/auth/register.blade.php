@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="first-name" class="col-md-4 col-form-label text-md-right">{{ __('messages.first_name') }}</label>

                            <div class="col-md-6">
                                <input id="first-name" type="text" class="form-control @error('first-name') is-invalid @enderror" name="first-name" value="{{ old('first-name') }}" required autocomplete="first-name" autofocus>

                                @error('first-name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="last-name" class="col-md-4 col-form-label text-md-right">{{ __('messages.last_name') }}</label>

                            <div class="col-md-6">
                                <input id="last-name" type="text" class="form-control @error('last-name') is-invalid @enderror" name="last-name" value="{{ old('last-name') }}" required autocomplete="last-name" autofocus>

                                @error('last-name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="linkedin-profile" class="col-md-4 col-form-label text-md-right">{{ __('messages.linkedin_profile_url') }}</label>

                            <div class="col-md-6">
                                <input id="linkedin-profile" type="text" class="form-control @error('linkedin-profile') is-invalid @enderror" name="linkedin-profile" value="{{ old('linkedin-profile') }}" required autocomplete="linkedin-profile" autofocus>

                                @error('linkedin-profile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="company" class="col-md-4 col-form-label text-md-right">{{ __('messages.company') }}</label>

                            <div class="col-md-6">
                                <select class="custom-select{{ $errors->has('company') ? ' is-invalid' : '' }}" required id="company" name="company">
                                    <option value="" selected>{{ __('messages.choose_company') }}</option>
                                    @foreach($companies as $company)
                                        <option value="{{ $company->id }}">{{ $company->company }}</option>
                                    @endforeach
                                </select>

                                @error('company'))
                                    <div class="invalid-feedback" role="alert">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="job-title" class="col-md-4 col-form-label text-md-right">{{ __('messages.job_title') }}</label>

                            <div class="col-md-6">
                                <select class="custom-select{{ $errors->has('job-title') ? ' is-invalid' : '' }}" required id="job-title" name="job-title">
                                    <option value="" selected>{{ __('messages.choose_job_title') }}</option>
                                    @foreach($jobTitles as $title)
                                        <option value="{{ $title->id }}">{{ $title->job_title }}</option>
                                    @endforeach
                                </select>

                                @error('job-title'))
                                    <div class="invalid-feedback" role="alert">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="skill" class="col-md-4 col-form-label text-md-right">{{ __('messages.skill') }}</label>

                            <div class="col-md-6">
                                <select class="custom-select{{ $errors->has('skill') ? ' is-invalid' : '' }}" required id="skill" name="skill">
                                    <option value="" selected>{{ __('messages.choose_skill') }}</option>
                                    @foreach($skills as $skill)
                                        <option value="{{ $skill->id }}">{{ $skill->skill }}</option>
                                    @endforeach
                                </select>

                                @error('skill'))
                                    <div class="invalid-feedback" role="alert">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="layoff-date" class="col-md-4 col-form-label text-md-right">{{ __('messages.layoff_date') }}</label>

                            <div class="col-md-6">
                                <input id="layoff-date" type="text" class="form-control @error('layoff-date') is-invalid @enderror" name="layoff-date" value="{{ old('layoff-date') }}" required autocomplete="layoff-date" autofocus>

                                @error('layoff-date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="total-experience" class="col-md-4 col-form-label text-md-right">{{ __('messages.total_experience') }}</label>

                            <div class="col-md-6">
                                <input id="total-experience" type="text" class="form-control @error('total-experience') is-invalid @enderror" name="total-experience" value="{{ old('total-experience') }}" required autocomplete="total-experience" autofocus>

                                @error('total-experience')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('messages.country') }}</label>

                            <div class="col-md-6">
                                <select class="custom-select{{ $errors->has('country') ? ' is-invalid' : '' }}" required id="country" name="country">
                                    <option value="" selected>{{ __('messages.choose_country') }}</option>
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country->country }}</option>
                                    @endforeach
                                </select>

                                @error('country'))
                                    <div class="invalid-feedback" role="alert">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" id="state-div" style="display:none">
                            <label for="state" class="col-md-4 col-form-label text-md-right">{{ __('messages.state') }}</label>

                            <div class="col-md-6">
                                <select class="custom-select{{ $errors->has('state') ? ' is-invalid' : '' }}" required id="state" name="state">
                                    <option value="" selected>{{ __('messages.choose_state') }}</option>
                                </select>

                                @error('state'))
                                    <div class="invalid-feedback" role="alert">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" id="city-div" style="display:none">
                            <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('messages.city') }}</label>

                            <div class="col-md-6">
                                <select class="custom-select{{ $errors->has('city') ? ' is-invalid' : '' }}" required id="city" name="city">
                                    <option value="" selected>{{ __('messages.choose_city') }}</option>
                                </select>

                                @error('city'))
                                    <div class="invalid-feedback" role="alert">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="resume" class="col-md-4 col-form-label text-md-right">{{ __('messages.resume') }}</label>

                            <div class="col-md-6">
                                <input id="resume" type="file" name="resume" class="form-control" required>

                                @error('resume')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="confirm-password" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="confirm-password" type="password" class="form-control" name="confirm-password" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function() {
        $("#layoff-date").datepicker({dateFormat:'yy-mm-dd'});

        $('#country').change(function() {console.log('EHRE');
            var $state = $('#state');
            $.ajax({
                url: "{{ route('states.index') }}",
                data: {
                    country: $(this).val()
                },
                success: function(data) {
                    $state.html('<option value="" selected>Choose State</option>');
                    $.each(data, function(id, value) {
                        $state.append('<option value="'+id+'">'+value+'</option>');
                    });
                    $('#state-div').show(150);
                }
            });
        });
        $('#state').change(function() {
            var $city = $('#city');
            $.ajax({
                url: "{{ route('cities.index') }}",
                data: {
                    state: $(this).val()
                },
                success: function(data) {
                    $city.html('<option value="" selected>Choose city</option>');
                    $.each(data, function(id, value) {
                        $city.append('<option value="'+id+'">'+value+'</option>');
                    });
                    $('#city-div').show(150);
                }
            });
        });
    });
</script>
@endsection
