@extends('layouts.default')

@section('content')

<div class="col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content">
    <h3>Login</h3>

    <form method="POST" name="frm-login">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="email">{{ __('messages.email') }}</label>
            <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" />
            @if($errors->first('email') != '')<span class="text-danger">{{ $errors->first('email') }}</span>@endif
        </div>

        <div class="form-group">
            <label for="password">{{ __('messages.password') }}</label>
            <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" />
            @if($errors->first('password') != '')<span class="text-danger">{{ $errors->first('password') }}</span>@endif
        </div>

        <button type="submit" class="btn btn-primary">Login</button>
        <!-- onclick="return userSignUp();" -->
    </form>
</div>
@endsection
