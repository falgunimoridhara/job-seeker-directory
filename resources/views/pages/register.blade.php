@extends('layouts.default')

@section('content')

<div class="col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content">
    <h3>Register</h3>

    <form method="POST" name="frm-register">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="first-name">{{ __('messages.first_name') }}</label>
            <input type="text" class="form-control" id="first-name" name="first-name" value="{{ old('first-name') }}" />
            @if($errors->first('first-name') != '')<span class="text-danger">{{ $errors->first('first-name') }}</span>@endif
        </div>

        <div class="form-group">
            <label for="last-name">{{ __('messages.last_name') }}</label>
            <input type="text" class="form-control" id="last-name" name="last-name" value="{{ old('last-name') }}" />
            @if($errors->first('last-name') != '')<span class="text-danger">{{ $errors->first('last-name') }}</span>@endif
        </div>

        <div class="form-group">
            <label for="linkedin-profile">{{ __('messages.linkedin_profile_url') }}</label>
            <input type="text" class="form-control" id="linkedin-profile" name="linkedin-profile" value="{{ old('linkedin-profile') }}" />
            @if($errors->first('linkedin-profile') != '')<span class="text-danger">{{ $errors->first('linkedin-profile') }}</span>@endif
        </div>

        <div class="form-group">
            <label for="layoff-date">{{ __('messages.layoff_date') }}</label>
            <input type="text" class="form-control" id="layoff-date" name="layoff-date" value="{{ old('layoff-date') }}" />
            @if($errors->first('layoff-date') != '')<span class="text-danger">{{ $errors->first('layoff-date') }}</span>@endif
        </div>

        <div class="form-group">
            <label for="total-experience">{{ __('messages.total_experience') }}</label>
            <input type="text" class="form-control" id="total-experience" name="total-experience" value="{{ old('total-experience') }}" />
            @if($errors->first('total-experience') != '')<span class="text-danger">{{ $errors->first('total-experience') }}</span>@endif
        </div>

        <div class="form-group">
            <label for="email">{{ __('messages.email') }}</label>
            <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" />
            @if($errors->first('email') != '')<span class="text-danger">{{ $errors->first('email') }}</span>@endif
        </div>

        <div class="form-group">
            <label for="password">{{ __('messages.password') }}</label>
            <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" />
            @if($errors->first('password') != '')<span class="text-danger">{{ $errors->first('password') }}</span>@endif
        </div>

        <div class="form-group">
            <label for="confirm-password">{{ __('messages.confirm_password') }}</label>
            <input type="password" class="form-control" id="confirm-password" name="confirm-password" value="{{ old('confirm-password') }}" />
            @if($errors->first('confirm-password') != '')<span class="text-danger">{{ $errors->first('confirm-password') }}</span>@endif
        </div>

        <button type="submit" class="btn btn-primary">Register</button>
        <!-- onclick="return userSignUp();" -->
    </form>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function() {
            $("#layoff-date").datepicker({dateFormat:'yy-mm-dd'});
        });
    </script>
</div>

@endsection
